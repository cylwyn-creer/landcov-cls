from __future__ import print_function, division

import os
import time
import torch
import pprint
import random
import argparse
import datetime
import torchvision

import dateutil.tz

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from colorama import Fore, init, deinit
from trainer import ClassifierModelTrainer
from torchvision import datasets, transforms

from misc.config import cfg
from misc.utils import print_log, VisdomPlotter

def parse_args():
  parser = argparse.ArgumentParser(description='Land Cover Classifier')

  parser.add_argument('--data_dir', dest='data_dir', type=str, default='data/ucmerced-land-use',
                      help='path where dataset samples are located')
  parser.add_argument('--gpu_id', dest='gpu_id', type=int, metavar='N', default=-1,
                      help='id of GPU to use')
  parser.add_argument('--op', dest='op', type=str, default='train',
                      help='operation to perform [`train` | `test`]')
  parser.add_argument('--model_name', dest='model_name', type=str, default='resnet',
                      help='model to use to build the classifier')
  parser.add_argument('--model_type', dest='model_type', type=str, default='finetuner',
                      help='type of training strategy for the model [`finetuner` | `extractor`]')
  parser.add_argument('--exec_name', dest='exec_name', type=str, default='landcovcls',
                      help='name of script execution')
  parser.add_argument('--manual_seed', dest='manual_seed', type=int, metavar='N', default=100000,
                      help='manual seed to use to make reproducible results')
  parser.add_argument('--model', dest='model', type=str, default=None,
                      help='pretrained model to be used for inference')
  parser.add_argument('--plotter', dest='plotter', type=str, default=None,
                      help='environment name for VisdomPlotter')

  args = parser.parse_args()
  return args

if __name__ == '__main__':
  # enable colored text logs
  init()

  print_log(Fore.CYAN, '[*] Land Cover Classifier')

  # parse command line arguments
  args = parse_args()

  # override configs if any
  if args.model is not None:
    cfg.MODEL.PRETRAINED = args.model

  # check command line arguments for invalid input
  ops = ['train', 'test']
  model_names = ['resnet', 'alexnet', 'vgg', 'squeezenet', 'densenet', 'inception']
  model_types = ['finetuner', 'extractor']

  if args.op not in ops:
    print_log(Fore.RED, '[!]', 'Invalid model type! Possible inputs:')
    pprint.pprint(ops)
    sys.exit()

  if args.model_name not in model_names:
    print_log(Fore.RED, '[!]', 'Invalid model name! Possible inputs:')
    pprint.pprint(model_names)
    sys.exit()

  if args.model_type not in model_types:
    print_log(Fore.RED, '[!]', 'Invalid model type! Possible inputs:')
    pprint.pprint(model_types)
    sys.exit()

  # setup device to use
  if args.gpu_id != -1:
    if torch.cuda.is_available():
      device = torch.device('cuda')
      print_log(Fore.MAGENTA, '[i]', 'Operating on GPU')
    else:
      device = torch.device('cpu')
      print_log(Fore.RED, '[!]', 'CUDA is not available! Operating on CPU instead')
  else:
    device = torch.device('cpu')
    print_log(Fore.MAGENTA, '[i]', 'Operating on CPU')

  # apply manual seed
  random.seed(args.manual_seed)
  np.random.seed(args.manual_seed)
  torch.manual_seed(args.manual_seed)
  if args.gpu_id != -1:
    torch.cuda.manual_seed_all(args.manual_seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

  # display command line arguments
  print_log(Fore.MAGENTA, '[i]', 'Command line arguments:')
  pprint.pprint(vars(args))

  # display configs
  print_log(Fore.MAGENTA, '[i]', 'Configurations used:')
  pprint.pprint(cfg)

  # setup output directory
  now = datetime.datetime.now(dateutil.tz.tzlocal())
  timestamp = now.strftime('%Y_%m_%d_%H_%M_%S')
  output_dir = 'output/{}_{}'.format(args.exec_name, timestamp)
  print_log(Fore.MAGENTA, '[i]', 'Saving all outputs at ' + output_dir)

  # setup GUI for displaying visual results
  if args.plotter is not None:
    plotter = VisdomPlotter(args.plotter, 'envs/{}_{}.log'.format(args.exec_name, timestamp))
  else:
    plotter = None

  # setup trainer
  trainer = ClassifierModelTrainer(device, plotter)

  # perform operation on model
  start_time = time.time()
  if args.op == 'train':
    # setup ouput directories
    trainer.set_output_dirs(output_dir)
    # initialize model to use
    model, input_size = trainer.initialize_model(args.model_name, args.model_type)

    # data augmentation and normalization
    data_transform = {
      'train': transforms.Compose([
        transforms.RandomResizedCrop(input_size),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
      ]),
      'test': transforms.Compose([
        transforms.Resize(input_size),
        transforms.CenterCrop(input_size),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
      ]),
    }

    # setup dataset and dataloader
    dataset = {x: datasets.ImageFolder(os.path.join(args.data_dir, x), data_transform[x]) for x in ops}
    assert dataset
    dataloader = {x: torch.utils.data.DataLoader(dataset[x], batch_size=cfg.DATA.BATCH_SIZE, shuffle=True,
                                                 num_workers=cfg.DATA.NUM_WORKERS) for x in ops}
    class_names = dataset[args.op].classes
    print_log(Fore.MAGENTA, '[i]', 'Number of samples: ' + str(len(dataset[args.op])))
    print_log(Fore.MAGENTA, '[i]', 'Number of batches: ' + str(len(dataloader[args.op])))
    
    # train model
    trainer.train(model, args.model_name, args.model_type, dataloader, class_names)

    # compute total training time    
    elapsed_time = time.time() - start_time
    hours = int(elapsed_time / 3600)
    elapsed_time = elapsed_time - (hours * 3600)
    minutes = int(elapsed_time / 60)
    time_log = 'Total time for training: {} hour/s and {} minute/s'.format(hours, minutes)
    print()
    print_log(Fore.MAGENTA, '[i]', time_log)
  else:
    # test model
    trainer.set_output_dirs(output_dir)
    # trainer.test()

    # compute total training time    
    elapsed_time = time.time() - start_time
    hours = int(elapsed_time / 3600)
    elapsed_time = elapsed_time - (hours * 3600)
    minutes = int(elapsed_time / 60)
    time_log = 'Total time for testing: {} hour/s and {} minute/s'.format(hours, minutes)
    print()
    print_log(Fore.MAGENTA, '[i]', time_log)

  # save logs to file
  log_file = os.path.join(output_dir, 'logs.txt')
  with open(log_file, 'w') as f:
    f.write('[i] Command line arguments:\n')
    pprint.pprint(vars(args), stream=f)
    f.write('[i] Configurations used:\n')
    pprint.pprint(cfg, stream=f)
    f.write('[i] Logs:\n')
    f.writelines(trainer.logs)
    f.write('\n')
    f.write(time_log)
  print_log(Fore.MAGENTA, '[i]', 'Logs stored at ' + log_file)

  # save results dictionary as csv file if it exist
  if trainer.results is not None:
    for key in trainer.results.keys():
      results_csv_file = os.path.join(output_dir, 'results_{}.csv'.format(key))
      dataframe = pd.DataFrame(trainer.results[key], columns=['train', 'test'])
      dataframe.to_csv(results_csv_file, index=False)
      print_log(Fore.MAGENTA, '[i]', 'Results stored at ' + results_csv_file)

  print_log(Fore.MAGENTA, '[i]', 'Operation completed succesfully!')

  # disable colored text logs
  deinit()
