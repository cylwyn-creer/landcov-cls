from __future__ import print_function, division

import os
import copy
import time
import torch
import datetime

import dateutil.tz

import numpy as np

import torch.nn as nn
import torch.optim as optim

from colorama import Fore
from torchvision import models

from misc.config import cfg
from misc.utils import mkdir, print_log, UnNormalize

def set_parameters_requires_grad(model, model_type):
  if model_type == 'extractor':
    for param in model.parameters():
      param.requires_grad = False

def get_timestamp():
  now = datetime.datetime.now(dateutil.tz.tzlocal())
  timestamp = now.strftime('[%Y:%m:%d::%H:%M:%S]')

  return timestamp

def get_snapshot(loss, acc, epoch, num_epochs, is_testing=False):
  snapshot = 'loss = {:5.4f} | acc = {:3.2f}'.format(loss, acc)

  if is_testing:
    snapshot = '[{:3d}/{:3d}][VAL] {}'.format(epoch + 1, num_epochs, snapshot)
  else:
    snapshot = '[{:3d}/{:3d}][TRN] {}'.format(epoch + 1, num_epochs, snapshot)

  return get_timestamp(), snapshot

class ClassifierModelTrainer(object):
  def __init__(self, device, plotter):
    self.device = device
    self.plotter = plotter
    self.results = {'loss': [], 'acc': []}
    self.logs = []

  def set_output_dirs(self, output_dir):
    self.models_dir = os.path.join(output_dir, 'models')
    mkdir(self.models_dir)

  # def test(self):
  #   model, optimizer, lr_scheduler, criterion, start_epoch, loss = self.build_models()
  #   unorm = UnNormalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))

  #   print_log(Fore.CYAN, '[i] Testing classifier model...\n')
  #   try:
  #     # perform inference with validation dataset to check for overfitting
  #     test_result = {'loss': [], 'acc': []}
  #     model.eval()
  #     ctr = 0
  #     with torch.no_grad():
  #       for step, data in enumerate(self.dataloader['test'], 1):
  #         inputs = data[0].to(self.device)
  #         labels = data[1].to(self.device)

  #         outputs = model(inputs)
  #         _, preds = torch.max(outputs, 1)
  #         loss = criterion(outputs, labels)

  #         test_result['loss'].append(loss.item())
  #         test_result['acc'].append(torch.sum(preds == labels.data).item() / inputs.size(0))

  #         # show test example
  #         for i in range(inputs.size(0)):
  #           title = '{}: {} ? {}'.format(ctr, self.class_names[labels[i]], self.class_names[preds[i]])
  #           print_log(Fore.GREEN, get_timestamp(), title)

  #           if self.plotter is not None:
  #             self.plotter.image_plot(ctr, unorm(inputs.cpu().data[i]), title)
  #           ctr += 1
  #       timestamp, snapshot = get_snapshot(test_result, start_epoch, cfg.TRAIN.MAX_EPOCH)
  #       print_log(Fore.GREEN, timestamp, snapshot)
  #       self.logs.append(timestamp + snapshot)
  #   except KeyboardInterrupt:
  #     print_log(Fore.RED, '[!]', 'Exiting from testing early...')

  def initialize_model(self, model_name, model_type, use_pretrained=True):
    model = None
    input_size = 0

    if model_name == 'resnet':
      model = models.resnet18(pretrained=use_pretrained)
      set_parameters_requires_grad(model, model_type)
      num_ftrs = model.fc.in_features
      model.fc = nn.Linear(num_ftrs, cfg.DATA.NUM_CLASSES)
      input_size = 224

    elif model_name == 'alexnet':
      model = models.alexnet(pretrained=use_pretrained)
      set_parameters_requires_grad(model, model_type)
      num_ftrs = model.classifier[6].in_features
      model.classifier[6] = nn.Linear(num_ftrs,cfg.DATA.NUM_CLASSES)
      input_size = 224

    elif model_name == 'vgg':
      model = models.vgg11_bn(pretrained=use_pretrained)
      set_parameters_requires_grad(model, model_type)
      num_ftrs = model.classifier[6].in_features
      model.classifier[6] = nn.Linear(num_ftrs,cfg.DATA.NUM_CLASSES)
      input_size = 224

    elif model_name == 'squeezenet':
      model = models.squeezenet1_0(pretrained=use_pretrained)
      set_parameters_requires_grad(model, model_type)
      model.classifier[1] = nn.Conv2d(512, cfg.DATA.NUM_CLASSES, kernel_size=(1,1), stride=(1,1))
      model.num_classes = cfg.DATA.NUM_CLASSES
      input_size = 224

    elif model_name == 'densenet':
      model = models.densenet121(pretrained=use_pretrained)
      set_parameters_requires_grad(model, model_type)
      num_ftrs = model.classifier.in_features
      model.classifier = nn.Linear(num_ftrs, cfg.DATA.NUM_CLASSES)
      input_size = 224

    elif model_name == 'inception':
      model = models.inception_v3(pretrained=use_pretrained)
      set_parameters_requires_grad(model, model_type)
      num_ftrs = model.AuxLogits.fc.in_features
      model.AuxLogits.fc = nn.Linear(num_ftrs, cfg.DATA.NUM_CLASSES)
      num_ftrs = model.fc.in_features
      model.fc = nn.Linear(num_ftrs, cfg.DATA.NUM_CLASSES)
      input_size = 299

    return model, input_size

  def train(self, model, model_name, model_type, dataloader, class_names):
    unorm = UnNormalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
    model = model.to(self.device)

    # initialize the optimizer with the model's trainable parameters
    params_to_update = model.parameters()
    if model_type == 'extractor':
      params_to_update = []
      for param in model.parameters():
        if param.requires_grad == True:
          params_to_update.append(param)
    optimizer = optim.SGD(params_to_update, lr=cfg.TRAIN.LR, momentum=cfg.TRAIN.MOMENTUM)

    # setup loss function
    criterion = nn.CrossEntropyLoss()

    # train the model
    print_log(Fore.CYAN, '[i] Training classifier model...\n')
    try:
      train_loss = None
      train_acc = None
      for epoch in range(cfg.TRAIN.MAX_EPOCH):
        for phase in ['train', 'test']:
          if phase == 'train':
            model.train()       # set model to training mode
          else:
            model.eval()        # set model to evaluate mode

          running_loss = 0.0
          running_corrects = 0

          # iterate over data
          for step, data in enumerate(dataloader[phase], 1):
            inputs = data[0].to(self.device)
            labels = data[1].to(self.device)

            # zero the parameter gradients
            optimizer.zero_grad()

            # track history during training only
            with torch.set_grad_enabled(phase == 'train'):
              # get model outputs and calculate loss
              if model_name == 'inception' and phase == 'train':
                outputs, aux_outputs = model(inputs)
                loss1 = criterion(outputs, labels)
                loss2 = criterion(aux_outputs, labels)
                loss = loss1 + 0.4 * loss2
              else:
                outputs = model(inputs)
                loss = criterion(outputs, labels)
              _, preds = torch.max(outputs, 1)

              # back propagate and optimize during training phase only
              if phase == 'train':
                loss.backward()
                optimizer.step()

              # if testing, use first batch to show as example
              if step == 1 and phase == 'test':
                if self.plotter is not None:
                  for i in range(inputs.size(0)):
                    title = '{}: {} ? {}'.format(i, class_names[labels[i]], class_names[preds[i]])
                    self.plotter.image_plot(i, unorm(inputs.cpu().data[i]), title)

            running_loss += loss.item() * inputs.size(0)
            running_corrects += torch.sum(preds == labels.data)

          epoch_loss = running_loss / len(dataloader[phase].dataset)
          epoch_acc = running_corrects.double().item() / len(dataloader[phase].dataset)

          # show statistics
          timestamp, snapshot = get_snapshot(epoch_loss, epoch_acc, epoch, cfg.TRAIN.MAX_EPOCH, phase == 'test')
          print_log(Fore.GREEN, timestamp, snapshot)
          self.logs.append(timestamp + snapshot)

          if self.plotter is not None:
            # see how the model is doing by comparing the training and validation results using a line graph
            self.plotter.line_plot('loss', phase, 'Land Cover Classification', epoch, epoch_loss, 'epoch', 'loss')
            self.plotter.line_plot('acc', phase, 'Land Cover Classification', epoch, epoch_acc, 'epoch', 'acc')

          # save plottable values to results dictionary for future plotting
          if phase == 'train':
            train_loss = epoch_loss
            train_acc = epoch_acc
          else:
            self.results['loss'].append([train_loss, epoch_loss])
            self.results['acc'].append([train_acc, epoch_acc])

        # create resumable checkpoint
        if epoch % cfg.TRAIN.CHECKPOINT_INTERVAL == 0:
          checkpoint = {
            'model_state_dict': model.state_dict(),
          }
          torch.save(checkpoint, '{}/{}_cls_{}_{}.pth'.format(self.models_dir, model_name, model_type, epoch))

        # create checkpoint to be used for inference
        if epoch == (cfg.TRAIN.MAX_EPOCH - 1):
          checkpoint = {
            'model_state_dict': model.state_dict(),
          }
          torch.save(checkpoint, '{}/{}_cls_{}.pth'.format(self.models_dir, model_name, model_type))
    except KeyboardInterrupt:
      print_log(Fore.RED, '[!]', 'Exiting from testing early...')