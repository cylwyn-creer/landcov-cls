@echo off
::
:: Note: Visdom server should be running before running these scripts.
::       Remove --plotter if visdom is not running.
::
:: train script for finetuning image classification models
:: python main.py --gpu_id=0 --plotter=ft1
:: python main.py --gpu_id=0 --model_name=alexnet --plotter=ft2
:: python main.py --gpu_id=0 --model_name=vgg --plotter=ft3
:: python main.py --gpu_id=0 --model_name=squeezenet --plotter=ft4
:: python main.py --gpu_id=0 --model_name=densenet --plotter=ft5
:: python main.py --gpu_id=0 --model_name=inception --plotter=expft6
::
:: train script for feature exractor image classification models
python main.py --gpu_id=0 --model_type=extractor --plotter=fe1
python main.py --gpu_id=0 --model_name=alexnet --model_type=extractor --plotter=fe2
python main.py --gpu_id=0 --model_name=vgg --model_type=extractor --plotter=fe3
python main.py --gpu_id=0 --model_name=squeezenet --model_type=extractor --plotter=fe4
python main.py --gpu_id=0 --model_name=densenet --model_type=extractor --plotter=fe5
python main.py --gpu_id=0 --model_name=inception --model_type=extractor --plotter=fe6