from easydict import EasyDict

__C = EasyDict()
cfg = __C

# data configs
__C.DATA = EasyDict()
__C.DATA.NUM_WORKERS = 4
__C.DATA.BATCH_SIZE = 64
__C.DATA.NUM_CLASSES = 21

# pretrained model configs
__C.MODEL = EasyDict()
__C.MODEL.PRETRAINED = None

# training configs
__C.TRAIN = EasyDict()
__C.TRAIN.MAX_EPOCH = 30
__C.TRAIN.SNAPSHOT_INTERVAL = 1
__C.TRAIN.CHECKPOINT_INTERVAL = 5
__C.TRAIN.LR = 0.001
__C.TRAIN.MOMENTUM = 0.9
__C.TRAIN.STEP_SIZE = 7
__C.TRAIN.GAMMA = 0.1
