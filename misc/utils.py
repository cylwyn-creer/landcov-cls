from __future__ import print_function

import os
import errno

import numpy as np

from visdom import Visdom
from colorama import Style, Fore

# print log with colored prefices
def print_log(log_type, prefix, content=None):
  """
    Fore.GREEN => timestamp
    Fore.RED => error
    Fore.BLUE => header
    Fore.CYAN => title
    Fore.MAGENTA => information
  """
  print(log_type + prefix, end=' ')
  if content is not None:
    print(Style.RESET_ALL + content)
  else:
    print(Style.RESET_ALL)

# create directory with the specified path
def mkdir(path):
  try:
    os.makedirs(path)
  except OSError as exc:
    if exc.errno == errno.EEXIST and os.path.isdir(path):
      pass
    else:
      raise

# unormalize image tensors
class UnNormalize(object):
  def __init__(self, mean, std):
    self.mean = mean
    self.std = std

  def __call__(self, tensor):
    for t, m, s in zip(tensor, self.mean, self.std):
      t.mul_(s).add_(m)
    return tensor

# plot and draw results using the visdom package
class VisdomPlotter(object):
  def __init__(self, env_name, log_file):
    self.viz = Visdom(env=env_name, log_to_filename=log_file)
    self.env = env_name
    self.plots = {}

  def line_plot(self, var_name, split_name, title, x, y, xlabel, ylabel):
    if var_name not in self.plots:
      self.plots[var_name] = self.viz.line(X=np.array([x, x]), Y=np.array([y, y]), env=self.env, opts=dict(
        legend=[split_name],
        title=title,
        xlabel=xlabel,
        ylabel=ylabel
      ))
    else:
      self.viz.line(X=np.array([x]), Y=np.array([y]), env=self.env, win=self.plots[var_name],
                    name=split_name, update='append')

  def image_plot(self, var_name, image, title):
    if var_name not in self.plots:
      self.plots[var_name] = self.viz.image(image, opts=dict(title=title))
    else:
      self.viz.image(image, env=self.env, win=self.plots[var_name], opts=dict(title=title))

  def images_plot(self, var_name, images):
    if var_name not in self.plots:
      self.plots[var_name] = self.viz.images(images, env=self.env)
    else:
      self.viz.images(images, env=self.env, win=self.plots[var_name])

  def text_plot(self, var_name, text):
    if var_name not in self.plots:
      self.plots[var_name] = self.viz.text(text, env=self.env)
    else:
      self.viz.text(text, env=self.env, win=self.plots[var_name])